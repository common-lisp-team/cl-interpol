(require "asdf")

(let ((asdf:*user-cache* (uiop:getenv "AUTOPKGTEST_TMP"))) ; Store FASL in some temporary dir
  (asdf:load-system "cl-interpol")
  (asdf:load-system "cl-interpol/test"))

;; Can't use ASDF:TEST-SYSTEM, its return value is meaningless
(unless (cl-interpol-test:run-all-tests)
  (uiop:quit 1))
